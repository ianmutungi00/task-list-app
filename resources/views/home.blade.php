@extends('layouts.master')

@section('content')
    <div class="text-center">
        <h1><i style="font-size:3.5em;" class="fa fa-bitbucket"></i></br> Welcome to Bucket List</h1>
        <hr/>

        @include('partials.flash_notification')

      
    </div>
@endsection